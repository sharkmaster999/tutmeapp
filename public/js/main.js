$(document).ready(function() {
    $(".add-btn").on("click", function () {
        $("#addModal").modal("show");
    });

    $(".add-btn1").on("click", function () {
        $("#addModal1").modal("show");
    });

    $(".remove-btn").on("click", function () {
        var id = $(this).data("id");

        $(".item_id").val(id);

        $("#removeModal").modal("show");
    });


    // Schedule

    $(".update-sched-trigger").on("click", function () {
        var id = $(this).data("id");
        var t_start = $(this).data("start");
        var t_end = $(this).data("end");
        var daysched = $(this).data("daysched");

        $(".item_id").val(id);
        $(".time_start").val(t_start);
        $(".time_end").val(t_end);
        $(".daysched").val(daysched);

        $("#updateModal").modal("show");
    });
});