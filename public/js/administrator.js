$(document).ready(function() {
    $(".add-cart-btn").on("click", function () {
        $("#addCartModal").modal("show");
    });

    $(".get-data-update-btn").on("click", function() {
        $(".upd-item-id").val($(this).data("id"));
        $(".upd-name").val($(this).data("name"));
        $(".upd-price").val($(this).data("price"));
        $("#updateItemModal").modal('show');
    });

    $(".upd-item-btn").on("click", function () {
        $(".upd-item-frm").submit();
    });

    $(".get-data-del-btn").on("click", function () {
        $(".del-item-id").val($(this).data("id"));
        $(".del-name").text($(this).data("name"));
        $("#deleteItemModal").modal('show');
    });

    $(".del-item-btn").on("click", function () {
        $(".del-item-frm").submit();
    });


    // upgrades

    $(".add-upg-trigger-btn").on("click", function () {
        $("#addUpgradeModal").modal('show');
    });

    $(".add-upg-btn").on("click", function () {
        $(".add-upg-frm").submit();
    });

    $(".get-data-update-upg-btn").on("click", function () {
        $(".upd-upg-id").val($(this).data('id'));
        $(".upd-upg-item").val($(this).data('itemid'));
        $(".upd-upg-item").text($(this).data('name'));
        $(".upd-upg-size").val($(this).data('size'));
        $(".upd-upg-price").val($(this).data('price'));
        $("#editUpgradeModal").modal('show');
    });

    $(".edit-upg-btn").on("click", function () {
        $(".edit-upg-frm").submit();
    });

    $(".get-data-del-upg-btn").on("click", function () {
        $(".del-upg-id").val($(this).data('id'));
        $(".del-upg-itemname").text($(this).data('name'));
        $(".del-upg-size").text($(this).data('size'));
        $("#deleteUpgradeModal").modal('show');
    });

    $(".del-upg-btn").on('click', function () {
        $('.del-upg-frm').submit();
    });

    $(".add-account-btn").on("click", function () {
        $(".add-account-frm").submit();
    });
});
