$(document)
    .ready(function() {
        $('.ui.menu .ui.dropdown').dropdown({
            on: 'hover'
        });

        $(".add-queue-btn").on("click", function () {
            $("#addToQueue").modal("show");
        });
        $('.ui.checkbox')
            .checkbox()
        ;

        $(".show-sidebar").on("click", function () {
            $('.ui.sidebar')
                .sidebar('toggle')
            ;
        });

        $(".success-artcart").on("click", function () {
            $("#addCartModal").modal("hide");
            $("#successAddCart").modal("show");
        });

        $(".add-item-btn").on("click", function () {
            $(".add-item-frm").submit();
        });
    });
;