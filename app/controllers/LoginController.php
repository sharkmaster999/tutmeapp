<?php

class LoginController extends Controller
{
    public function index()
    {
        $this->view('login/index');
    }

    public function logout()
    {
        Session::destroy();
        header("Location: ".URL."login");
    }

    public function login() {
        $username = $_POST["username"];
        $password = $_POST["password"];

        $user = $this->model("User");
        $checkIfExist = $user->ifExist($username, $password);

        if($checkIfExist > 0) {
            $role_id = $user->getRoleID($username, $password);
            $type = $user->getType($username, $password);

            $_SESSION["role_id"] = $role_id;
            $_SESSION["username"] = $username;
            $_SESSION["password"] = $password;
            $_SESSION["type"] = $type;

            if($type == "Tutor")
                header("Location: ".URL."tutor");
            else if($type == "Student")
                header("Location: ".URL."student");
        } else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }



}
