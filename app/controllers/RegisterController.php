<?php

class RegisterController extends Controller {
    public function index() {
        $this->view("register/index");
    }

    public function new_user() {
        $firstname = $_POST["firstname"];
        $lastname = $_POST["lastname"];
        $middlename = $_POST["middlename"];
        $username = $_POST["username"];
        $password = $_POST["password"];
        $r_address = $_POST["r_address"];
        $p_address = $_POST["p_address"];
        if(isset($_POST["istutor"]))
            $type = "Tutor";
        else
            $type = "Student";

        $user = $this->model('User');
        $user->addAccount($username, $password, $type, $firstname, $middlename, $lastname, $r_address, $p_address);
        $user_id = $user->getRoleID($username, $password);
        $user->addAbout($user_id, "Description here needs to update", date("Y"), "default-avatar.png");

        $_SESSION["role_id"] = $user_id;
        $_SESSION["username"] = $username;
        $_SESSION["password"] = $password;
        $_SESSION["type"] = $type;

        if($type == "Tutor")
            header("Location: ".URL."tutor");
        else if($type == "Student")
            header("Location: ".URL."student");
    }
}