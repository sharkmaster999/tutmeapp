<?php

class TutorController extends Controller {

    public function isTutor() {
        $isTutor = 0;
        if($_SESSION["type"] == "Tutor")
            $isTutor = 1;

        return $isTutor;
    }
    public function index() {
        if($this->isTutor())
            $this->view('tutor/index');
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    public function account() {
        if($this->isTutor())
            $this->view('tutor/account');
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    public function students() {
        if($this->isTutor())
            $this->view('tutor/students');
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    public function schedules() {
        if($this->isTutor())
            $this->view('tutor/schedules');
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    // Schedule

    public function addSchedule()
    {
        if($this->isTutor())
        {
            $user_id = $_SESSION["role_id"];
            $day = $_POST["day"];
            $start = $_POST["start"];
            $end = $_POST["end"];

            $schedule = $this->model("Schedule");
            $schedule->add($user_id, $start, $end, $day);
            header("Location: ".URL."tutor/schedules");
        }
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    public function removeSchedule() {
        if($this->isTutor())
        {
            $sched_id = $_POST["id"];

            $schedule = $this->model("Schedule");
            $schedule->remove($sched_id);
            header("Location: ".URL."tutor/schedules");
        }
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    public function updateSchedule() {
        if($this->isTutor())
        {
            $sched_id = $_POST["id"];
            $day = $_POST["day"];
            $start = $_POST["start"];
            $end = $_POST["end"];

            $schedule = $this->model("Schedule");
            $schedule->update($sched_id, $start, $end, $day);
            header("Location: ".URL."tutor/schedules");
        }
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    // Expertise

    public function addExpertise() {
        if($this->isTutor())
        {
            $tutor_id = $_SESSION["role_id"];
            $name = $_POST["expertise"];

            $expertise = $this->model("Expertise");
            $expertise->add($tutor_id, $name);
            header("Location: ".URL."tutor/account");
        }
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    public function removeExpertise() {
        if($this->isTutor())
        {
            $exp_id = $_POST["id"];

            $expertise = $this->model("Expertise");
            $expertise->remove($exp_id);
            header("Location: ".URL."tutor/account");
        }
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    // Account

    public function updateAccount() {
        if($this->isTutor())
        {
            $firstname = $_POST["firstname"];
            $lastname = $_POST["lastname"];
            $middlename = $_POST["middlename"];
            $username = $_POST["username"];
            $password = $_POST["password"];
            $r_address = $_POST["r_address"];
            $p_address = $_POST["p_address"];

            $user = $this->model('User');
            $user->updateAccount($username, $password, $type, $firstname, $middlename, $lastname, $r_address, $p_address);

            $_SESSION["username"] = $username;
            $_SESSION["password"] = $password;

            header("Location: ".URL."tutor/account");
        }
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    public function updateAbout(){
        if($this->isTutor())
        {
            $image = $_FILES["image"]["name"];
            $about_id = $_POST["id"];
            $bio = $_POST["bio"];

            $target_dir = "public/img/profile/";
            $target_file = $target_dir . basename($_FILES["image"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

            if(isset($_POST["submit"])) {
                $check = getimagesize($_FILES["image"]["tmp_name"]);
                if($check !== false) {
                    echo "File is an image - " . $check["mime"] . ".";
                    $uploadOk = 1;
                } else {
                    echo "File is not an image.";
                    $uploadOk = 0;
                }
            }

            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
            } else {
                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                    echo "The file ". basename( $_FILES["image"]["name"]). " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }

            $user = $this->model("User");
            if($image == "")
                $user->updateAboutWoImage($about_id, $bio);
            else
                $user->updateAbout($about_id, $bio, $image);
            //echo $target_dir;
            header("Location: ".URL."tutor/account");
        }
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

}