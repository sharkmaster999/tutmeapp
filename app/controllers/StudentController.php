<?php

class StudentController extends Controller {
    
	public function isStudent() {
        $isStudent = 0;
        if($_SESSION["type"] == "Student")
            $isStudent = 1;

        return $isStudent;
    }

    public function index() {
        if($_SESSION["type"] == "Student")
            $this->view("student/index");
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    public function account()
    {
    	if($_SESSION["type"] == "Student")

            $this->view("student/account");
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    // Account

    public function updateAccount() {
        if($this->isStudent())
        {
            $firstname = $_POST["firstname"];
            $lastname = $_POST["lastname"];
            $middlename = $_POST["middlename"];
            $username = $_POST["username"];
            $password = $_POST["password"];
            $r_address = $_POST["r_address"];
            $p_address = $_POST["p_address"];

            $user = $this->model('User');
            $user->updateAccount($username, $password, $type, $firstname, $middlename, $lastname, $r_address, $p_address);

            $_SESSION["username"] = $username;
            $_SESSION["password"] = $password;

            header("Location: ".URL."student/account");
        }
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    public function tutor_search_page() {
        if($this->isStudent() > 0)
        {
            $this->view('student/tutor_search');
       	}else {
            Session::destroy();
            header("Location: ".URL."login");
       }
    }

    public function hiring_tutor($data = '')
    {
    	if($this->isStudent())
            $this->view('student/hiring_tutor', $data);
       	else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    public function search_tutor() {
        if($this->isStudent()) {
            $_SESSION["query_opt"] = $_POST["query_opt"];
            $_SESSION["key"] = $_POST["key"];
            $this->view('student/search_result');
        }
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }

    public function updateAbout(){
        if($this->isStudent())
        {
            $image = $_FILES["image"]["name"];
            $about_id = $_POST["id"];
            $bio = $_POST["bio"];

            $target_dir = "public/img/profile/";
            $target_file = $target_dir . basename($_FILES["image"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

            if(isset($_POST["submit"])) {
                $check = getimagesize($_FILES["image"]["tmp_name"]);
                if($check !== false) {
                    echo "File is an image - " . $check["mime"] . ".";
                    $uploadOk = 1;
                } else {
                    echo "File is not an image.";
                    $uploadOk = 0;
                }
            }

            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
            } else {
                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                    echo "The file ". basename( $_FILES["image"]["name"]). " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }

            $user = $this->model("User");
            if($image == "")
                $user->updateAboutWoImage($about_id, $bio);
            else
                $user->updateAbout($about_id, $bio, $image);
            header("Location: ".URL."student/account");
        }
        else {
            Session::destroy();
            header("Location: ".URL."login");
        }
    }
}