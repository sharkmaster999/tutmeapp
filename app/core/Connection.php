<?php

class Connection
{
    protected $host = "localhost";
    protected $user = "root";
    protected $pass = "";
    protected $db_name = "tutorial";
    protected $dbh = null;

    public function openConnection()
    {
        try
        {
            $this->dbh = new PDO("mysql:host=".$this->host.";dbname=".$this->db_name, $this->user, $this->pass);

        } catch(Exception $e)
        {
            $e->getMessage();
        }

        return $this->dbh;
    }

    public function closeConnection()
    {
        try
        {
            $this->dbh = null;
        } catch(Exception $e)
        {
            $e->getMessage();
        }

        return $this->dbh;
    }


}