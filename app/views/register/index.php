<?php include_once 'header.php'?>
<div class="ui container">
    <br><br>
    <h2 class="ui violet image header">
        <img src="<?=URL?>public/img/logo.png" class="image">
        <div class="content">
            TutMeApp
        </div>
    </h2>
    <div class="ui stackable equal width grid">
        <div class="column">
            <h1>Registration</h1>
            <p>
                Register now and hire world-class tutor who are expert in their different fields.
            </p>
            <form class="ui form" method="POST" action="<?=URL?>register/new_user">
                <h4 class="ui dividing header">Personal Information</h4>
                <div class="field">
                    <label>Name</label>
                    <div class="three fields">
                        <div class="field">
                            <input type="text" name="lastname" placeholder="Last Name" autocomplete="off">
                        </div>
                        <div class="field">
                            <input type="text" name="firstname" placeholder="First Name" autocomplete="off">
                        </div>
                        <div class="field">
                            <input type="text" name="middlename" placeholder="Middle Name" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        <input type="text" name="username" placeholder="Username" autocomplete="off">
                    </div>
                    <div class="field">
                        <input type="password" name="password" placeholder="Password" autocomplete="off">
                    </div>
                </div>
                <h4 class="ui dividing header">Payment Information</h4>
                <div class="field">
                    <label>Billing Address</label>
                    <div class="two fields">
                        <div class="field">
                            <textarea type="text" name="p_address" rows="2" placeholder="Permanent Address"></textarea>
                        </div>
                        <div class="field">
                            <textarea type="text" name="r_address" rows="2" placeholder="Residential Address"></textarea>
                        </div>
                    </div>
                </div>
                <div class="ui segment">
                    <div class="field">
                        <div class="ui toggle checkbox">
                            <input type="checkbox" name="istutor" tabindex="0" class="hidden">
                            <label>I want to be a tutor and earn.</label>
                        </div>
                    </div>
                </div>
                <button class="ui violet button" type="submit" tabindex="0">Register Now</button>
                <div class="ui message">
                    Already have your account? <a href="<?= URL;?>login">Login Now</a>
                </div>
            </form>
        </div>
    </div>

    <br><br><br>
</div>