<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>TutMeApp | Register</title>
    <link rel="stylesheet" href="<?php echo Server::appRoot(); ?>public/css/semantic.css"/>
    <link rel="stylesheet" href="<?php echo Server::appRoot(); ?>public/css/semantic-customize.css"/>
    <script type="text/javascript" src="<?php echo Server::appRoot(); ?>public/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo Server::appRoot(); ?>public/js/semantic.js"></script>
    <script type="text/javascript" src="<?php echo Server::appRoot(); ?>public/js/semantic-scripts.js"></script>
</head>
<body>
<div class="ui violet two item inverted menu labeled icon fixed">
    <a href="<?=URL?>" class="item labeled">
        <i class="home icon"></i> Home
    </a>

    <a href="<?=URL?>login" class="item labeled">
        <i class="power icon"></i> Log-in
    </a>
</div>
