<?php include_once 'header.php'?>
<div class="ui container">
    <?php include_once 'side-menu.php'?>
    <?php
        $user_data = Controller::model('User')->getData($data);
        $about = Controller::model('About')->getData($data);
    ?>
    <br><br><br>
    <h2 class="ui violet image header">
        <img src="<?=URL?>public/img/final-logo.png" class="image">
        <div class="content">
            TutMeApp for Student
        </div>
    </h2>
    <h3 class="ui dividing header">Personal Information</h3>
    <div class="ui three column grid stackable">
        <div class="column">
            <div class="ui centered card">
                <div class="image">
                    <img style="width: 100%; height: 250px;" src="<?=URL?>public/img/profile/<?=$about["image"]?>">
                </div>
                <div class="content">
                    <a class="header"><?= ucfirst($user_data["firstname"])?> <?= ucfirst($user_data["lastname"])?></a>
                    <div class="meta">
                        <span class="date">Joined in <?=$about["year_joined"]?></span>
                    </div>
                    <div class="description">
                        <?=$about["bio"]?>
                    </div>
                </div>
                <div class="extra content">
                    <button class="ui tiny violet button"><i class="check icon"></i> Hire this tutor</button>
                </div>
            </div>
        </div>
        <div class="column">
            <h3 class="ui dividing header">Testimonies</h3>
            <div class="ui comments">
                <div class="comment">
                    <a class="avatar">
                        <img src="<?=URL?>public/img/joe.jpg">
                    </a>
                    <div class="content">
                        <a class="author">Remuelito Remulta</a>
                        <div class="metadata">
                            <div class="date">2 days ago</div>
                            <div class="rating">
                                <i class="star icon"></i>
                                5 Faves
                            </div>
                        </div>
                        <div class="text">
                            She is nice tutor, in detail with her discussions.
                        </div>
                    </div>
                </div>
                <div class="comment">
                    <a class="avatar">
                        <img src="<?=URL?>public/img/joe.jpg">
                    </a>
                    <div class="content">
                        <a class="author">Reynante Precilda</a>
                        <div class="metadata">
                            <div class="date">1 day ago</div>
                            <div class="rating">
                                <i class="star icon"></i>
                                4 Faves
                            </div>
                        </div>
                        <div class="text">
                            She needs more preparation.
                        </div>
                    </div>
                </div>
                <div class="comment">
                    <a class="avatar">
                        <img src="<?=URL?>public/img/joe.jpg">
                    </a>
                    <div class="content">
                        <a class="author">Remuelito Remulta</a>
                        <div class="metadata">
                            <div class="date">2 days ago</div>
                            <div class="rating">
                                <i class="star icon"></i>
                                5 Faves
                            </div>
                        </div>
                        <div class="text">
                            She is nice tutor, in detail with her discussions.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <h3 class="ui dividing header">Dashboard</h3>
            <div style="width: auto; height: auto;">
                <canvas id="myChart"></canvas>
            </div>

        </div>
    </div>

    <script>
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["Feb 23", "Feb 24", "Feb 25", "Feb 26", "Feb 27", "Feb 28"],
                datasets: [{
                    label: '# of Students Teach',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>
</div>