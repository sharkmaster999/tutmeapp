<?php include_once 'header.php'?>
<?php include_once 'side-menu.php'?>
<div class="ui container">
    <br><br><br>
    <h2 class="ui violet image header">
        <img src="<?=URL?>public/img/final-logo.png" class="image">
        <div class="content">
            TutMeApp for Student
        </div>
    </h2>
    <h3 class="ui dividing header">Search for Tutors</h3>

    <form action="<?=URL?>student/search_tutor" method="POST" class="ui form">
        <div class="two fields">
            <div class="two wide field">
                <select class="ui fluid search dropdown" name="query_opt">
                    <option value="Tutor">By Tutor</option>
                    <option value="Expertise">By Expertise</option>
                </select>
            </div>
            <div class="six wide field">
                <div class="ui icon input">
                    <input type="text" placeholder="Search..." name="key" value="<?= $_SESSION["key"]?>" autocomplete="off">
                    <i class="inverted violet circular search link icon"></i>
                </div>
            </div>
        </div>
    </form>
    <br>
    <h3 class="ui dividing header">Search result for <?= $_SESSION["query_opt"]?> in key  "<?= $_SESSION["key"]?>"</h3>
    <div class="ui stackable three column grid">

        <?php
            if($_SESSION["query_opt"] == "Tutor")
                Controller::model('Tutor')->viewAllTutorsByName($_SESSION["key"]);
            else
                Controller::model('Tutor')->viewAllTutorsByExpertise($_SESSION["key"]);

            unset($_SESSION["key"]);
            unset($_SESSION["query_opt"]);
        ?>

    </div>

</div>