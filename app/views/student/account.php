<?php include_once 'header.php'?>
<?php include_once 'side-menu.php'?>
<div class="ui container">

    <?php
        $user_data = Controller::model('User')->getData($_SESSION["role_id"]);
        $about = Controller::model('About')->getData($_SESSION["role_id"]);
    ?>
    <br><br><br>
    <h2 class="ui violet image header">
        <img src="<?=URL?>public/img/final-logo.png" class="image">
        <div class="content">
            TutMeApp for Student
        </div>
    </h2>
    <div class="ui grid stackable">
        <div class="five wide column">
            <h3 class="ui dividing header">Introduction</h3>
            <div class="ui centered card">
                <div class="image">
                    <img src="<?=URL?>public/img/profile/<?=$about["image"]?>">
                </div>
                <div class="content">
                    <a class="header"><?= ucfirst($user_data["firstname"])?> <?= ucfirst($user_data["lastname"])?></a>
                    <div class="meta">
                        <span class="date">Joined in <?=$about["year_joined"]?></span>
                    </div>
                    <div class="description">
                        <?=$about["bio"]?>
                    </div>
                </div>
                <div class="extra content">
                    <a>
                        <i class="user icon"></i>
                        22 Topics
                    </a>
                    <a class="text align right add-btn" href="#"><i class="pencil icon"></i> Update Me</a>
                </div>
            </div>
        </div>
        <div class="eleven wide column">
            <form class="ui form" method="POST" action="<?=URL?>student/updateAccount">
                <h3 class="ui dividing header">Personal Information</h3>
                <div class="field">
                    <div class="three fields">
                        <div class="field">
                            <label>Last Name:</label>
                            <input type="text" name="lastname" value="<?= ucfirst($user_data["lastname"])?>" autocomplete="off">
                        </div>
                        <div class="field">
                            <label>First Name:</label>
                            <input type="text" name="firstname" value="<?= ucfirst($user_data["firstname"])?>" autocomplete="off">
                        </div>
                        <div class="field">
                            <label>Middle Name:</label>
                            <input type="text" name="middlename" value="<?= ucfirst($user_data["middlename"])?>" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        <label>Username:</label>
                        <input type="text" name="username" value="<?= $_SESSION["username"]?>" autocomplete="off">
                    </div>
                    <div class="field">
                        <label>Password:</label>
                        <input type="password" name="password" value="<?= $_SESSION["password"]?>" autocomplete="off">
                    </div>
                </div>
                <h3 class="ui dividing header">Payment Information</h3>
                <div class="field">
                    <div class="field">
                        <label>Permanent Address:</label>
                        <textarea type="text" name="p_address" rows="1"><?= $user_data["p_address"]?></textarea>
                    </div>
                    <div class="field">
                        <label>Residential Address:</label>
                        <textarea type="text" name="r_address" rows="1"><?= $user_data["r_address"]?></textarea>
                    </div>
                </div>
                <button class="ui violet button" type="submit" tabindex="0"><i class="pencil icon"></i> Update My Account</button>
            </form>
        </div>
    </div>

    <div class="ui tiny modal" id="addModal">
        <div class="header">Update About Me</div>
        <div class="content">
            <form action="<?=URL?>student/updateAbout" method="POST" class="ui form upd-item-frm" enctype="multipart/form-data">
                <div class="field">
                    <label>Short description:</label>
                    <input type="hidden" name="id" value="<?=$about["id"];?>"/>
                    <input type="text" name="bio" value="<?=$about["bio"];?>" autocomplete="off">
                </div>
                <div class="field">
                    <label>Choose Image File:</label>
                    <input type="file" name="image" autocomplete="off">
                </div>
            </form>
        </div>
        <div class="actions">
            <button class="ui small violet button upd-item-btn"><i class="pencil icon"></i>Update</button>
            <button class="ui small red cancel button"><i class="times icon"></i>Cancel</button>
        </div>
    </div>
</div>
