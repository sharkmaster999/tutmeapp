<?php include_once 'header.php'?>
<div class="ui container">
    <br><br><br>
    <h2 class="ui violet image header">
        <img src="<?=URL?>public/img/final-logo.png" class="image">
        <div class="content">
            TutMeApp for Student
        </div>
    </h2>
    <div class="ui raised segment">
        <div class="ui two column grid stackable">
            <div class="column">

                <img src="<?=URL?>public/img/image.jpg" class="image" style="width: 100%;">
            </div>
            <div class="column">
                <br>
                <p style="font-size: 19px; padding: 10px;">
                    <b>TutMeApp</b> is a web application designed for managing profiles. It is intended to developed community of tutors and
                    students interact. With as features helps community to expands its development, enhancing its capabilities,
                    improves communication better. TutMe helps clients desire to learned new ways, and  guide them better.
                    <br><br>
                    <a href="<?= URL;?>register" class="ui violet huge button">Start TutMe App Now!</a>
                </p>
            </div>
        </div>
    </div>
    
    <?php include_once 'side-menu.php'?>
</div>
