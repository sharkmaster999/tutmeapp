<div class="ui left demo vertical inverted sidebar labeled icon menu">
    <a class="item" href="<?=URL?>student">
        <i class="home icon"></i>
        Home
    </a>
    <a class="item" href="<?=URL?>student/tutor_search_page">
        <i class="search icon"></i>
        Search Tutor
    </a>
    <a class="item" href="<?=URL?>student/my_tutors">
        <i class="users icon"></i>
        My Tutors
    </a>
    <a class="item" href="<?=URL?>student/schedules">
        <i class="list icon"></i>
        Schedules
    </a>
    <a class="item" href="<?=URL?>login/logout">
        <i class="power icon"></i>
        Logout
    </a>
</div>
