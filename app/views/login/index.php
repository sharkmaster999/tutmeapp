<?php include_once 'header.php'?>
<div class="ui container">
    <br><br><br><br><br><br><br><br>
    <div class="ui stackable equal width grid">
        <div class="column"></div>
        <div class="column">
            <div class="column">
                <h2 class="ui violet image header">
                    <img src="<?=URL?>public/img/final-logo.png" class="image">
                    <div class="content">
                        Welcome to TutMeApp
                    </div>
                </h2>
                <form class="ui large form" action="<?= URL;?>login/login" method="POST">

                    <div class="ui stacked segment">
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="user icon"></i>
                                <input type="text" name="username" placeholder="Username" autocomplete="off">
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="lock icon"></i>
                                <input type="password" name="password" placeholder="Password" autocomplete="off">
                            </div>
                        </div>
                        <button type="submit" class="ui fluid large violet submit button" >Login</button>
                    </div>

                    <div class="ui error message"></div>

                </form>

                <div class="ui message">
                    New to us? <a href="<?= URL;?>register">Register Now</a>
                </div>
            </div>
        </div>
        <div class="column"></div>
    </div>

    <div class="ui middle aligned center aligned grid">

    </div>

    <br><br><br>
</div>