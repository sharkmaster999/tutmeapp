<?php include_once 'header.php'?>
<?php include_once 'side-menu.php'?>
<div class="ui container">
    <br><br><br>
    <h2 class="ui violet image header">
        <img src="<?=URL?>public/img/final-logo.png" class="image">
        <div class="content">
            TutMeApp
        </div>
    </h2>
    <h2 class="ui dividing header">Schedule Time</h2>
    <button class="ui violet tiny button add-btn"><i class="plus icon"></i>Add Schedule</button>
    <table class="ui celled padded table">
        <thead>
            <tr>
                <th>Day</th>
                <th>Time Start</th>
                <th>Time End</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
                Controller::model('Schedule')->view($_SESSION["role_id"]);
            ?>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="5">
                    <div class="ui right floated pagination menu">
                        <a class="icon item">
                            <i class="left chevron icon"></i>
                        </a>
                        <a class="item">1</a>
                        <a class="item">2</a>
                        <a class="item">3</a>
                        <a class="item">4</a>
                        <a class="icon item">
                            <i class="right chevron icon"></i>
                        </a>
                    </div>
                </th>
            </tr>
        </tfoot>
    </table>

    <!--//Add-->
    <div class="ui tiny modal" id="addModal">
        <div class="header">Add New Schedule</div>
        <div class="content">
            <form action="<?=URL?>tutor/addSchedule" method="POST" class="ui form add-item-frm">
                <div class="field">
                    <div class="three fields">
                        <div class="field">
                            <label>Day:</label>
                            <input type="text" name="day" autocomplete="off">
                        </div>
                        <div class="field">
                            <label>Time Start:</label>
                            <input type="text" name="start" autocomplete="off">
                        </div>
                        <div class="field">
                            <label>Time End:</label>
                            <input type="text" name="end" autocomplete="off">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="actions">
            <button class="ui small violet button add-item-btn"><i class="plus icon"></i>Add Schedule</button>
            <button class="ui small red cancel button"><i class="times icon"></i>Cancel</button>
        </div>
    </div>

    <!--// Remove-->
    <div class="ui tiny modal" id="removeModal">
        <div class="header">Remove Schedule</div>
        <div class="content">
            <form action="<?=URL?>tutor/removeSchedule" method="POST" class="ui form del-item-frm">
                <input type="hidden" class="item_id" name="id" value="">
                Are you sure you want to remove this time schedule?
            </form>
        </div>
        <div class="actions">
            <button class="ui small red button del-item-btn"><i class="trash icon"></i>Remove</button>
            <button class="ui small blue cancel button"><i class="times icon"></i>Cancel</button>
        </div>
    </div>

    <!--// Update-->
    <div class="ui tiny modal" id="updateModal">
        <div class="header">Update Schedule</div>
        <div class="content">
            <form action="<?=URL?>tutor/updateSchedule" method="POST" class="ui form upd-item-frm">
                <div class="field">
                    <div class="two fields">
                        <div class="field">
                            <label>Day:</label>
                            <input type="text" class="daysched" value="" name="day" autocomplete="off">
                        </div>
                        <div class="field">
                            <label>Time Start:</label>
                            <input type="hidden" class="item_id" name="id" value="">
                            <input type="text" class="time_start" name="start" autocomplete="off">
                        </div>
                        <div class="field">
                            <label>Time End:</label>
                            <input type="text" class="time_end" name="end" autocomplete="off">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="actions">
            <button class="ui small violet button upd-item-btn"><i class="pencil icon"></i>Update</button>
            <button class="ui small red cancel button"><i class="times icon"></i>Cancel</button>
        </div>
    </div>

</div>
