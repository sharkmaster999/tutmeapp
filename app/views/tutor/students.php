<?php include_once 'header.php'?>
<div class="ui container">
    <br><br><br>
    <h2 class="ui violet image header">
        <img src="<?=URL?>public/img/final-logo.png" class="image">
        <div class="content">
            TutMeApp for Student
        </div>
    </h2>

    <h3 class="ui dividing header">Student Requests</h3>
    <div class="ui cards">
        <div class="card">
            <div class="content">
                <img class="right circular floated mini ui image" src="<?=URL?>public/img/default-avatar.png">
                <div class="header">
                    Elliot Fu
                </div>
                <div class="meta">
                    Visayas State University
                </div>
                <div class="description">
                    Elliot requested to hire you as her <b>tutor</b>
                </div>
            </div>
            <div class="extra content">
                <div class="ui two buttons">
                    <div class="ui basic green button">Approve</div>
                    <div class="ui basic red button">Decline</div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="content">
                <img class="right circular floated mini ui image" src="<?=URL?>public/img/default-avatar.png">
                <div class="header">
                    Jenny Hess
                </div>
                <div class="meta">
                    New Member
                </div>
                <div class="description">
                    Jenny requested to hire you as her <b>tutor</b>
                </div>
            </div>
            <div class="extra content">
                <div class="ui two buttons">
                    <div class="ui basic green button">Approve</div>
                    <div class="ui basic red button">Decline</div>
                </div>
            </div>
        </div>
    </div>

    <h3 class="ui dividing header">Active Clients</h3>
    <div class="ui cards">
        <div class="card">
            <div class="content">
                <div class="header">Elliot Fu</div>
                <div class="description">
                    Elliot Fu is a film-maker from New York.
                </div>
            </div>
            <div class="ui bottom attached button">
                <i class="add icon"></i>
                Add Friend
            </div>
        </div>
        <div class="card">
            <div class="content">
                <div class="header">Veronika Ossi</div>
                <div class="description">
                    Veronika Ossi is a set designer living in New York who enjoys kittens, music, and partying.
                </div>
            </div>
            <div class="ui bottom attached button">
                <i class="add icon"></i>
                Add Friend
            </div>
        </div>
        <div class="card">
            <div class="content">
                <div class="header">Jenny Hess</div>
                <div class="description">
                    Jenny is a student studying Media Management at the New School
                </div>
            </div>
            <div class="ui bottom attached button">
                <i class="add icon"></i>
                Add Friend
            </div>
        </div>
    </div>
    <?php include_once 'side-menu.php'?>
</div>
