<?php include_once 'header.php'?>
<?php include_once 'side-menu.php'?>
<div class="ui container">
    <?php
    $user_data = Controller::model('User')->getData($_SESSION["role_id"]);
    ?>
    <br><br><br>
    <h2 class="ui violet image header">
        <img src="<?=URL?>public/img/final-logo.png" class="image">
        <div class="content">
            TutMeApp
        </div>
    </h2>
    <div class="ui three column grid stackable">
        <?php
        $about = Controller::model('About')->getData($_SESSION["role_id"]);
        ?>
        <div class="column">
            <h3 class="ui dividing header">Introduction</h3>
            <div class="ui centered card">
                <div class="image">
                    <img src="<?=URL?>public/img/profile/<?=$about["image"]?>">
                </div>
                <div class="content">
                    <a class="header"><?= ucfirst($user_data["firstname"])?> <?= ucfirst($user_data["lastname"])?></a>
                    <div class="meta">
                        <span class="date">Joined in <?=$about["year_joined"]?></span>
                    </div>
                    <div class="description">
                        <?=$about["bio"]?>
                    </div>
                </div>
                <div class="extra content">
                    <a>
                        <i class="user icon"></i>
                        22 Students
                    </a>
                    <a class="text align right add-btn1" href="#"><i class="pencil icon"></i> Update Me</a>
                </div>
            </div>
        </div>
        <div class="column">
            <form class="ui form" method="POST" action="<?=URL?>tutor/updateAccount">
                <h3 class="ui dividing header">Personal Information</h3>
                <div class="field">
                    <div class="field">
                        <label>Last Name:</label>
                        <input type="text" name="lastname" value="<?= ucfirst($user_data["lastname"])?>" autocomplete="off">
                    </div>
                    <div class="field">
                        <label>First Name:</label>
                        <input type="text" name="firstname" value="<?= ucfirst($user_data["firstname"])?>" autocomplete="off">
                    </div>
                    <div class="field">
                        <label>Middle Name:</label>
                        <input type="text" name="middlename" value="<?= ucfirst($user_data["middlename"])?>" autocomplete="off">
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        <label>Username:</label>
                        <input type="text" name="username" value="<?= $_SESSION["username"]?>" autocomplete="off">
                    </div>
                    <div class="field">
                        <label>Password:</label>
                        <input type="password" name="password" value="<?= $_SESSION["password"]?>" autocomplete="off">
                    </div>
                </div>
                <h3 class="ui dividing header">Payment Information</h3>
                <div class="field">
                    <div class="field">
                        <label>Permanent Address:</label>
                        <textarea type="text" name="p_address" rows="1"><?= $user_data["p_address"]?></textarea>
                    </div>
                    <div class="field">
                        <label>Residential Address:</label>
                        <textarea type="text" name="r_address" rows="1"><?= $user_data["r_address"]?></textarea>
                    </div>
                </div>
                <button class="ui tiny violet button" type="submit" tabindex="0">Update</button>
            </form>
        </div>
        <div class="column">
            <h3 class="ui dividing header">Expertise</h3>
            <button class="ui violet tiny button add-btn"><i class="plus icon"></i>Add Expertise</button>
            <br><br>
            <div class="ui violet labels">

                <?php
                Controller::model('Expertise')->view($_SESSION["role_id"]);
                ?>

            </div>
        </div>
    </div>



    <div class="ui tiny modal" id="addModal">
        <div class="header">Add New Expertise</div>
        <div class="content">
            <form action="<?=URL?>tutor/addExpertise" method="POST" class="ui form add-item-frm">
                <div class="field">
                    <label>Expertise:</label>
                    <input type="text" name="expertise" autocomplete="off">
                </div>
            </form>
        </div>
        <div class="actions">
            <button class="ui small violet button add-item-btn"><i class="plus icon"></i>Add Expertise</button>
            <button class="ui small red cancel button"><i class="times icon"></i>Cancel</button>
        </div>
    </div>

    <div class="ui tiny modal" id="addModal1">
        <div class="header">Update About Me</div>
        <div class="content">
            <form action="<?=URL?>tutor/updateAbout" method="POST" class="ui form upd-item-frm" enctype="multipart/form-data">
                <div class="field">
                    <label>Short description:</label>
                    <input type="hidden" name="id" value="<?=$about["id"];?>"/>
                    <input type="text" name="bio" value="<?=$about["bio"];?>" autocomplete="off">
                </div>
                <div class="field">
                    <label>Choose Image File:</label>
                    <input type="file" name="image" autocomplete="off">
                </div>
            </form>
        </div>
        <div class="actions">
            <button class="ui small violet button upd-item-btn"><i class="pencil icon"></i>Update</button>
            <button class="ui small red cancel button"><i class="times icon"></i>Cancel</button>
        </div>
    </div>

    <div class="ui tiny modal" id="removeModal">
        <div class="header">Remove Expertise</div>
        <div class="content">
            <form action="<?=URL?>tutor/removeExpertise" method="POST" class="ui form del-item-frm">
                <input type="hidden" class="item_id" name="id" value="">
                Are you sure you want to remove this expertise?
            </form>
        </div>
        <div class="actions">
            <button class="ui small red button del-item-btn"><i class="trash icon"></i>Remove</button>
            <button class="ui small blue cancel button"><i class="times icon"></i>Cancel</button>
        </div>
    </div>
</div>
