<div class="ui left demo vertical inverted sidebar labeled icon menu">
    <a class="item" href="<?=URL?>tutor">
        <i class="home icon"></i>
        Home
    </a>
    <a class="item" href="<?=URL?>tutor/schedules">
        <i class="list icon"></i>
        Schedules
    </a>
    <a class="item" href="<?=URL?>tutor/students">
        <i class="users layout icon"></i>
        Students
    </a>
    <a class="item" href="<?=URL?>tutor">
        <i class="dollar icon"></i>
        Earnings
    </a>
    <a class="item" href="<?=URL?>login/logout">
        <i class="power icon"></i>
        Logout
    </a>
</div>
