<?php

class TutorModel extends Connection {

    function showExpertise($user_id) {
        try {
            $this->openConnection();
            $str = "";
            $sql = "SELECT name FROM expertise WHERE tutor_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $user_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $str .= '<a class="ui label">
                            '. $row["name"]. '
                        </a>';
            }

            $this->closeConnection();

            return $str;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }
    function viewAllTutors() {
        try {
            $this->openConnection();

            $sql = "SELECT r.id, r.type, u.firstname, u.lastname, u.middlename, a.image, a.bio
                    FROM roles r, users u, about a
                    WHERE r.type = 'Tutor' AND r.id = u.role_id  AND a.user_id = u.id";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<div class="column">
                        <div class="ui cards">
                            <div class="ui card">
                                <div class="content">
                                    <img class="right floated mini ui image" src="'.URL. 'public/img/profile/'. $row["image"]. '">
                                    <div class="header">
                                        '. $row["firstname"]. ' '. $row["lastname"]. '
                                    </div>
                                    <div class="meta">
                                        Tutoree
                                    </div>
                                    <div class="description">
                                        <p>'. $row["bio"]. '</p>
                                        <br>
                                        <div class="ui violet labels">
                                            '. $this->showExpertise($row["id"]). '
                                        </div>
                                    </div>
                                </div>
                                <div class="extra content">
                                    <div class="ui two buttons">
                                        <a href="'. URL. 'student/hiring_tutor/'. $row["id"]. '" class="ui violet button" >Hire Me Now</a>
                                        <div class="ui purple button">Full Profile</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    // Search tutor by name

    function viewAllTutorsByName($key) {
        try {
            $this->openConnection();

            $sql = "SELECT r.id, r.type, u.firstname, u.lastname, u.middlename, a.image, a.bio
                FROM roles r, users u, about a
                WHERE r.type = 'Tutor' AND r.id = u.role_id AND a.user_id = u.id
                AND (u.lastname LIKE '%". $key. "%'
                OR   u.firstname LIKE '%". $key. "%' OR u.middlename LIKE '%". $key. "%')";

            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<div class="column">
                        <div class="ui cards">
                            <div class="ui card">
                                <div class="content">
                                    <img class="right circular floated mini ui image" src="'.URL. 'public/img/profile/'. $row["image"]. '">
                                    <div class="header">
                                        '. $row["firstname"]. ' '. $row["lastname"]. '
                                    </div>
                                    <div class="meta">
                                        Software Developer
                                    </div>
                                    <div class="description">
                                        <p>'. $row["bio"]. '</p>
                                        <br>
                                        <div class="ui violet labels">
                                            '. $this->showExpertise($row["id"]). '
                                        </div>
                                    </div>
                                </div>
                                <div class="extra content">
                                    <div class="ui two buttons">
                                        <a href="'. URL. 'student/hiring_tutor/'. $row["id"]. '" class="ui violet button" >Hire Me Now</a>
                                        <div class="ui purple button">Full Profile</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    // Search tutor by expertise

    function viewAllTutorsByExpertise($key) {
        try {
            $this->openConnection();


            $sql = "SELECT r.id, r.type, u.firstname, u.lastname, u.middlename, a.image, a.bio
                    FROM roles r, users u, expertise e, about a
                    WHERE r.type = 'Tutor' AND r.id = u.role_id
                    AND e.tutor_id = r.id AND e.name LIKE '%". $key. "%' AND a.user_id = u.id";

            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<div class="column">
                        <div class="ui cards">
                            <div class="ui card">
                                <div class="content">
                                    <img class="right circular floated mini ui image" src="'.URL. 'public/img/profile/'. $row["image"]. '">
                                    <div class="header">
                                        '. $row["firstname"]. ' '. $row["lastname"]. '
                                    </div>
                                    <div class="meta">
                                        Software Developer
                                    </div>
                                    <div class="description">
                                        <p>'. $row["bio"]. '</p>
                                        <br>
                                        <div class="ui violet labels">
                                            '. $this->showExpertise($row["id"]). '
                                        </div>
                                    </div>
                                </div>
                                <div class="extra content">
                                    <div class="ui two buttons">
                                        <a href="'. URL. 'student/hiring_tutor/'. $row["id"]. '" class="ui violet button" >Hire Me Now</a>
                                        <div class="ui purple button">Full Profile</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
}