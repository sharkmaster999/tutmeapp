<?php
class TopicModel extends Connection
{
	function getByOwner($owner_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM topics WHERE owner_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $owner_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->closeConnection();

            return $res;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
}
?>