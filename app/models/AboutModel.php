<?php

class AboutModel extends Connection {
    function getData($user_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM about WHERE user_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $user_id);
            $stmt->execute();

            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();
            return $res;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
}