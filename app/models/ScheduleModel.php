<?php

class ScheduleModel extends Connection {
    function add($user_id, $start, $end, $day) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO schedules VALUES (0, ?, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $user_id);
            $stmt->bindParam(2, $start);
            $stmt->bindParam(3, $end);
            $stmt->bindParam(4, $day);
            $stmt->execute();

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function view($user_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM schedules WHERE tutor_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $user_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<tr>
                        <td>'. $row["day"]. '</td>
                        <td>'. $row["time_start"]. '</td>
                        <td>'. $row["time_end"]. '</td>
                        <td class="single line">
                            <button class="ui violet mini button update-sched-trigger"
                                    data-id="'. $row["id"]. '"
                                    data-start="'. $row["time_start"]. '"
                                    data-end="'. $row["time_end"]. '"
                                    data-daysched="'. $row["day"]. '">Update</button>
                            <button data-id="'. $row["id"]. '" class="ui red mini button remove-btn">Remove</button>
                        </td>
                    </tr>';
            }

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function remove($sched_id) {
        try {
            $this->openConnection();

            $sql = "DELETE FROM schedules WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $sched_id);
            $stmt->execute();

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function update($sched_id, $start, $end, $day) {
        try {
            $this->openConnection();

            $sql = "UPDATE schedules SET time_start = ?, time_end = ?, day = ? WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $start);
            $stmt->bindParam(2, $end);
            $stmt->bindParam(3, $day);
            $stmt->bindParam(4, $sched_id);
            $stmt->execute();

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }
}