<?php

class ExpertiseModel extends Connection {
    function add($tutor_id, $name) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO expertise VALUES (0, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $tutor_id);
            $stmt->bindParam(2, $name);
            $stmt->execute();

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }


    function view($tutor_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM expertise WHERE tutor_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $tutor_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<a class="ui label">
                        '. $row["name"]. ' <i class="icon close remove-btn" data-id="'. $row["id"]. '"></i>
                    </a>';
            }

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function remove($exp_id) {
        try {
            $this->openConnection();

            $sql = "DELETE FROM expertise WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $exp_id);
            $stmt->execute();

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }
}