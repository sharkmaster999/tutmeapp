<?php

class UserModel extends Connection
{

    function addRole($username, $password, $type)
    {
        try
        {
            $this->openConnection();

            $sql = "INSERT INTO roles VALUES (0, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $username);
            $stmt->bindParam(2, $password);
            $stmt->bindParam(3, $type);
            $stmt->execute();

            $this->closeConnection();
        }
        catch (Exception $e)
        {
            $e->getMessage();
        }
    }

    function getRoleID($username, $password) {
        try {
            $this->openConnection();

            $sql = "SELECT id FROM roles WHERE username = ? AND password = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $username);
            $stmt->bindParam(2, $password);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->closeConnection();

            return $res["id"];
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getType($username, $password) {
        try {
            $this->openConnection();

            $sql = "SELECT type FROM roles WHERE username = ? AND password = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $username);
            $stmt->bindParam(2, $password);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->closeConnection();

            return $res["type"];
        } catch (Exception $e) {
            $e->getMessage();
        }
    }


    function addAccount($username, $password, $type, $firstname, $middlename, $lastname, $r_address, $p_address) {
        try
        {
            $this->addRole($username, $password, $type);
            $role_id = $this->getRoleID($username, $password);
            $this->openConnection();

            $sql = "INSERT INTO users VALUES (0, ?, ?, ?, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $role_id);
            $stmt->bindParam(2, $firstname);
            $stmt->bindParam(3, $middlename);
            $stmt->bindParam(4, $lastname);
            $stmt->bindParam(5, $r_address);
            $stmt->bindParam(6, $p_address);
            $stmt->execute();

            $this->closeConnection();
        }
        catch (Exception $e)
        {
            $e->getMessage();
        }
    }

    function updateRole($username, $password)
    {
        try
        {
            $this->openConnection();
            $sql = "UPDATE roles SET username = ?, password = ?, type = ? WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $username);
            $stmt->bindParam(2, $password);
            $stmt->bindParam(3, $_SESSION['role_id']);
            $stmt->execute();

            $this->closeConnection();
        }
        catch (Exception $e)
        {
            $e->getMessage();
        }
    }

    function updateAccount($username, $password, $type, $firstname, $middlename, $lastname, $r_address, $p_address) {
        try
        {
            $this->updateRole($username, $password, $_SESSION["type"]);

            $this->openConnection();

            $sql = "UPDATE users SET firstname = ?, middlename = ?, lastname = ?, r_address = ?, p_address = ? WHERE role_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $firstname);
            $stmt->bindParam(2, $middlename);
            $stmt->bindParam(3, $lastname);
            $stmt->bindParam(4, $r_address);
            $stmt->bindParam(5, $p_address);
            $stmt->bindParam(6, $_SESSION['role_id']);
            $stmt->execute();

            $this->closeConnection();
        }
        catch (Exception $e)
        {
            $e->getMessage();
        }
    }

    function ifExist($username, $password) {
        try {
            $this->openConnection();

            $sql = "SELECT COUNT(*) AS ctr FROM roles WHERE username = ? AND password = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $username);
            $stmt->bindParam(2, $password);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->closeConnection();

            return $res["ctr"];
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function getData($role_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM users WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $role_id);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->closeConnection();

            return $row;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }


    function addAbout($user_id, $bio, $year_joined, $image) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO about VALUES (0, ?, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $user_id);
            $stmt->bindParam(2, $bio);
            $stmt->bindParam(3, $year_joined);
            $stmt->bindParam(4, $image);
            $stmt->execute();

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function updateAbout($about_id, $bio, $image) {
        try {
            $this->openConnection();

            $sql = "UPDATE about SET bio = ?, image = ? WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $bio);
            $stmt->bindParam(2, $image);
            $stmt->bindParam(3, $about_id);
            $stmt->execute();

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function updateAboutWoImage($about_id, $bio) {
        try {
            $this->openConnection();

            $sql = "UPDATE about SET bio = ? WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $bio);
            $stmt->bindParam(3, $about_id);
            $stmt->execute();

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }
}